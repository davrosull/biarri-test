import React, { Component } from 'react';
import PropTypes from 'prop-types';

import rd3 from 'rd3';

const LineChart = rd3.LineChart;

class Chart extends Component {
  static propTypes = {
    data: PropTypes.object,
    title: PropTypes.string,
  };

  render() {
    const lineData = [
      {
        name: 'Anger',
        values: this.props.data.anger,
        strokeWidth: 2,
      },

      {
        name: 'Disgust',
        values: this.props.data.disgust,
        strokeWidth: 2,
      },

      {
        name: 'Fear',
        values: this.props.data.fear,
        strokeWidth: 2,
      },
      {
        name: 'Joy',
        values: this.props.data.joy,
        strokeWidth: 2,
      },
      {
        name: 'Sadness',
        values: this.props.data.sadness,
        strokeWidth: 2,
      },
    ];

    return (
      <LineChart
        legend
        data={lineData}
        width={'90%'}
        height={500}
        legendOffset={200}
        viewBoxObject={{
          x: 0,
          y: 0,
          width: 800,
          height: 500,
        }}
        title={this.props.title}
        yAxisLabel="Rating"
        xAxisLabel="Acts & Scenes"
        // domain={{ x: [, 0], y: [-1] }}
        xAxisFormatter={x => ` ${this.props.data.labels[x]}`}
        gridHorizontal
        xAxisTickCount={this.props.data.labels.length}
      />
    );
  }
}

export default Chart;
