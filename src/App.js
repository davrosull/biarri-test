import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  dataLoaded,
  updateSpeechData,
  updateEmotionData,
} from './redux/actions';
import './App.css';
import Chart from './components/chart';
import _ from 'lodash';

// Setup Watson
var NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');
var natural_language_understanding = new NaturalLanguageUnderstandingV1({
  username: '40f79658-b96d-4727-8977-ab6009555045',
  password: '1do3laGScbQS',
  version_date: '2017-02-27',
});
class App extends Component {
  componentWillMount(props) {
    fetch('http://localhost:3000/henry_iv.json')
      .then(function(response) {
        return response.json();
      })
      .then(
        function(jsonData) {
          _.each(
            jsonData,
            function(d) {
              let data = d.line_number.split('.');
              if (data[1] !== undefined) {
                this.props.updateSpeechData({
                  ...d,
                  part: data[0],
                  scene: data[1],
                  key: `${data[0]}-${data[1]}`,
                });
              }
            }.bind(this)
          );

          this.props.dataLoaded();
        }.bind(this)
      )
      .catch(function(ex) {
        console.log('An error occured:', ex);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (
      !_.isEqual(nextProps.isDataLoaded, this.props.isDataLoaded) &&
      nextProps.isDataLoaded === true
    ) {
      _.each(nextProps.speeches, d => {
        natural_language_understanding.analyze(
          {
            text: _.map(d.text).join(' '),
            features: {
              entities: {
                emotion: true,
                sentiment: true,
                limit: 2,
              },
            },
          },
          function(error, response) {
            if (error) {
              console.log('An Error occured:', error);
            } else {
              this.props.updateEmotionData(response, d.key);
            }
          }.bind(this)
        );
      });
    }
  }

  render() {
    const { isDataLoaded, chartData } = this.props;
    return (
      <div className="App">
        <header className="App-header">
          <img
            src={
              'http://biarrirail.com/wp-content/uploads/2017/06/Biarri-Rail-Logo-Colour.png'
            }
            className="App-logo"
            alt="logo"
          />
          <h1 className="App-title">Henry IV's Emotions</h1>
        </header>
        <p className="App-intro">
          The chart below shows how the emotions change by Act and Scene
          according to the magic of IBM Watson Emotion Analysis!
        </p>
        {!isDataLoaded && (
          <div>
            <h1>Loading...</h1>
          </div>
        )}
        {isDataLoaded && (
          <Chart title={'Emotional Analysis'} data={chartData} />
        )}
        <div />
      </div>
    );
  }
}

const mapEmotion = (data, emotion) => {
  return _.map(_.map(data, emotion), (s, i) => ({ x: i, y: s ? s : 0 }));
};

const mapStateToProps = state => {
  const { speeches, isDataLoaded } = state.emotionData;
  let anger = mapEmotion(speeches, 'anger');
  let disgust = mapEmotion(speeches, 'disgust');
  let fear = mapEmotion(speeches, 'fear');
  let joy = mapEmotion(speeches, 'joy');
  let sadness = mapEmotion(speeches, 'sadness');

  let labels = _.map(speeches, 'key');

  const chartData = { anger, disgust, fear, joy, sadness, labels };

  return { speeches, isDataLoaded, anger, chartData };
};

const mapDispatchToProps = {
  dataLoaded,
  updateSpeechData,
  updateEmotionData,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
