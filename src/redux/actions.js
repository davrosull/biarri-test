import {
  LOAD_INITIAL_DATA,
  UPDATE_SPEECH_DATA,
  DATA_LOADED_SUCCESSFULLY,
  UPDATE_EMOTION_DATA,
} from './types';

export const loadInitialData = function(data) {
  return {
    type: LOAD_INITIAL_DATA,
    payload: data,
  };
};

export const dataLoaded = function() {
  return {
    type: DATA_LOADED_SUCCESSFULLY,
  };
};

export const updateSpeechData = function(data) {
  return {
    type: UPDATE_SPEECH_DATA,
    key: data.key,
    payload: data,
  };
};

export const updateEmotionData = function(data, key) {
  return {
    type: UPDATE_EMOTION_DATA,
    key: key,
    payload: data.entities[0] !== undefined ? data.entities[0].emotion : {},
  };
};
