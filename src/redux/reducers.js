//@flow

import { combineReducers } from 'redux';
import {
  LOAD_INITIAL_DATA,
  UPDATE_SPEECH_DATA,
  DATA_LOADED_SUCCESSFULLY,
  UPDATE_EMOTION_DATA,
} from './types';

const initialState = {
  data: {},
  speeches: {},
  chartData: {},
  isDataLoaded: false,
};

function emotionData(state = initialState, action) {
  switch (action.type) {
    case DATA_LOADED_SUCCESSFULLY:
      return { ...state, isDataLoaded: true };
    case LOAD_INITIAL_DATA:
      return { ...state, data: action.payload };
    case UPDATE_SPEECH_DATA:
      //   console.log(state.speeches[action.key].text_entry);
      const speechData = {
        ...action.payload,
        text:
          state.speeches[action.key] !== undefined
            ? {
                ...state.speeches[action.key].text,
                [action.payload.speech_number]: action.payload.text_entry,
              }
            : { [action.payload.speech_number]: action.payload.text_entry },
      };
      return {
        ...state,
        speeches: { ...state.speeches, [action.key]: speechData },
      };
    case UPDATE_EMOTION_DATA:
      return {
        ...state,
        speeches: {
          ...state.speeches,
          [action.key]: { ...action.payload, ...state.speeches[action.key] },
        },
      };

    default:
      return state;
  }
}

export default combineReducers({ emotionData });
