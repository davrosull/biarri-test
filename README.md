# David's Biarri Rail Front End Candidate Exercise

This project was bootstrapped with
[Create React App](https://github.com/facebookincubator/create-react-app)
(mainly for quick and simpleness).

## Getting Started

To run this project, run `yarn start`. The web-app will start loading the JSON
into the state and then will analyise the data and display a line graph showing
the results.

I've grouped each the play by scenes. This has given enough data points to be
interesting.

## Shortcuts taken

* No testing has been done
* Simplified the code structure, and minimised the redux boilerplating.
* Not using the compressed JSON
* No real error checking or handling
* Not really optimised
* Used a react library for the chart - vs coding it in d3 from scratch.

## Notes

I did stumble on the `watson-developer-cloud` module not wanting make a cross
domain request. I have
[a Chrome plugin](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi)
installed that has to work/develop without having to move it server side.

Alternatively you should be able to disable web security using `open
/Applications/Google\ Chrome.app/ --args --disable-web-security`

If you'd prefer - I can move the Watson calls to server side!

## Packages Used

* lodash - for JS utility and magic
* rd3 - A react component library for D3. Mainly used to simplify the process!
* react-redux - Used redux to manage the web-app state.
* watson-developer-cloud - To magically analyise the text for an emotional read
